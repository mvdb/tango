# Contributing to Tango

Contributions to Tango are much appreciated and are preferably handled
as follows:

* [Issues](https://gitlab.com/mvdb/tango/-/issues):
  This is a good place to report bugs, ask questions on how to use Tango
  and suggest improvements.

* [Merge requests](https://gitlab.com/mvdb/tango/-/merge_requests):
  MRs for bug fixes and enhancements should target the default branch,
  i.e. `master`. A source branch should ideally:

  * use [ASE-style commit acronyms](
    https://wiki.fysik.dtu.dk/ase/development/contribute.html#writing-the-commit-message),
  * present a [reasonably clean commit history](
    https://about.gitlab.com/blog/2018/06/07/keeping-git-commit-history-clean/),
  * be appropriately named (e.g. `feature_<somefeature>` or `bugfix_<somebug>`,
    not `master` or `main`),
  * describe relevant changes in the `CHANGELOG.md` file,
  * make sure that new functionality gets covered by the testsuite.

A list of contributors can be found in the [Contribution analytics](
https://gitlab.com/mvdb/tango/-/graphs/master) tab.
