# Release notes

## Development version


## Version 1.1.0

24 August 2024

* Added a `pyproject.toml` file for easier installation.

* By default the `DftbPlusCalculator` class now sets the total charge
  equal to the sum of initial atomic charges. This for example allows
  to include charged species in the repulsive potential fitting.

* Fixed a bug in the alphabetical ordering of the element pairs used
  as keys in the rmins and rcuts arguements to Tango.

* Repulsionless SKF input files are now by default expected to named
  ``*-*_offsite2c.skf``, which is the same template as in Hotcent v2.
  A ``norep_suffix`` keyword argument has been added to the TANGO class
  so that previously generated files (from e.g. Hotcent v1) can still
  be used without renaming (via e.g. ``norep_suffix='no_repulsion'``).

* Fixed a bug in the spline repulsion fitting procedure in which
  division-by-zero occured when one of the interatomic distances
  happened to overlap with one of the spline knots.

* Tango can now be used with a recent and well-defined ASE release (v3.23).

* The tests can now be run with [pytest](https://docs.pytest.org)
  and now behave in a deterministic/reproducible manner.

* The DFTB+ calculators now explicitly set the ParserVersion to 7
  to ensure compatibility across different DFTB+ versions.

* The CP2K calculator commands now assumed OpenMP builds of CP2K.

* The QuantumEspresso calculator wrapper has been deprecated
  and will be removed in the next release.


## Version 1.0

6 March 2020

* The (quite basic and slow) Slater-Koster table generation
  functionality has been removed as this is not the focus
  of Tango. Specialized codes should now be used to generate
  the required ``*-*_no_repulsion.skf`` files. These include
  [Hotbit](https://github.com/pekkosk/hotbit), the ONECENT
  and TWOCENT codes from the DFTB+ developers, and
  [Hotcent](https://gitlab.com/mvdb/hotcent).

* No more backwards compatibility with Python2 (only Python3).

* The `relax_alternate` function in tango.relax_utils has been
  renamed to `relax_standard`.


## Version 0.9

12 December 2019

* Start of versioning.
