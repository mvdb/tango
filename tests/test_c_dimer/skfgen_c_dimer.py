from ase.data import atomic_numbers, covalent_radii
from ase.units import Bohr
from hotcent.atomic_dft import AtomicDFT
from hotcent.confinement import PowerConfinement
from hotcent.offsite_twocenter import Offsite2cTable


def generate_skf():
    element = 'C'
    xc = 'LDA'
    configuration = '[He] 2s2 2p2'
    valence = ['2s', '2p']
    occupations = {'2s': 2, '2p': 2}

    # ------------------------------------
    # Compute eigenvalues of the free atom
    # ------------------------------------

    atom = AtomicDFT(element,
                     xc=xc,
                     configuration=configuration,
                     valence=valence,
                     scalarrel=False,
                     perturbative_confinement=False,
                     )
    atom.run()
    eigenvalues = {nl: atom.get_eigenvalue(nl) for nl in valence}

    # ---------------------------------------
    # Compute Hubbard values of the free atom
    # ---------------------------------------

    atom = AtomicDFT(element,
                     xc=xc,
                     configuration=configuration,
                     valence=valence,
                     scalarrel=False,
                     confinement=PowerConfinement(r0=40., s=4),
                     perturbative_confinement=False,
                     )
    atom.run()
    U = atom.get_hubbard_value('2p', scheme='central', maxstep=1.)
    hubbardvalues = {'2s': U, '2p': U}

    # -------------------------------
    # Compute Slater-Koster integrals
    # -------------------------------

    r_cov = covalent_radii[atomic_numbers[element]] / Bohr
    r_wfc = 2 * r_cov
    r_rho = 3 * r_cov
    atom = AtomicDFT(element,
                     xc=xc,
                     confinement=PowerConfinement(r0=r_wfc, s=2),
                     wf_confinement=PowerConfinement(r0=r_rho, s=2),
                     configuration=configuration,
                     valence=valence,
                     scalarrel=False,
                     perturbative_confinement=False,
                     )
    atom.run()

    # Compute Slater-Koster integrals:
    rmin, dr, N = 0.5, 0.05, 250
    off2c = Offsite2cTable(atom, atom)
    off2c.run(rmin, dr, N, superposition='density', xc=xc)
    off2c.write(eigenvalues=eigenvalues,
                hubbardvalues=hubbardvalues,
                occupations=occupations)

    return
