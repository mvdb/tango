"""C-C repulsion fitting test."""
import json
import os

import numpy as np
from ase import Atoms
from ase.calculators.emt import EMT
from ase.data import atomic_numbers, covalent_radii
from ase.db import connect

from tango import TANGO
from tango.calculators.dftbplus_calc import DftbPlusCalculator
from tango.run_utils import run_atom, run_dimers

from skfgen_c_dimer import generate_skf


class EMTCalculator(EMT):
    def __init__(self, atoms, kpts=(1, 1, 1), run_type=None):
        EMT.__init__(self, atoms=atoms)

    def exit(self):
        pass


class DftbPlusCalc(DftbPlusCalculator):
    def __init__(self, *args, **kwargs):
        kwargs['Hamiltonian_ShellResolvedSCC'] = 'No'
        kwargs['slako_dir'] = os.path.dirname(os.path.abspath(__file__)) + '/'
        DftbPlusCalculator.__init__(self, *args, **kwargs)


def generate_training_data(element, pair, dbext, dbfile, dimersdbfile,
                           atomicfile, rcov, mam):
    # Create training.<dbext>, dimers.<dbext> and atomic_energies.json
    for f in [dbfile, dimersdbfile, atomicfile]:
        if os.path.exists(f):
            os.remove(f)

    db = connect(dbfile)
    fractions = np.arange(0.85, 1.31, 0.05)
    for i, x in enumerate(fractions):
        atoms = Atoms([element] * 2, cell=[12.]*3, pbc=False,
                      positions=[[6.] * 3, [6. + 2 * x * rcov, 6., 6.]])
        calc = EMTCalculator(atoms)
        atoms.calc = calc
        atoms.get_potential_energy()
        db.write(atoms, relaxed=1)

    e_dft = run_atom(element, EMTCalculator)
    os.system('cp %s_offsite2c.skf %s.skf' % (pair, pair))
    e_dftb = run_atom(element, DftbPlusCalc, maximum_angular_momenta=mam)
    os.remove(pair + '.skf')
    atomic_energies = {'%s_DFT' % element: e_dft, '%s_DFTB' % element: e_dftb}
    with open(atomicfile, 'w') as f:
        json.dump(atomic_energies, f)

    db = connect(dimersdbfile)
    run_dimers(dimersdbfile, EMTCalculator, atomic_energies, element, element,
               minfrac=0.2, maxfrac=0.5, minE=-10.)
    return


def test_c_dimer():
    element = 'C'
    rcov = covalent_radii[atomic_numbers[element]]
    pair = '%s-%s' % (element, element)
    rcuts = {pair: 1.35 * 2 * rcov}
    mam = {element: 1}

    if not os.path.exists('C-C_offsite2c.skf'):
        generate_skf()

    for dbext in ['json', 'db']:
        dbfile = 'training.' + dbext
        dimersdbfile = 'dimers.' + dbext
        atomicfile = 'atomic_energies.json'
        generate_training_data(element, pair, dbext, dbfile, dimersdbfile,
                               atomicfile, rcov, mam)

        with open(atomicfile, 'r') as f:
            atomic_energies = json.load(f)

        for mode in ['poly', 'exp_poly', 'exp_spline']:
            rmins = {pair: 0.7 * 2 * rcov} if mode == 'exp_poly' else None
            powers = {pair: range(2, 7) if 'poly' in mode else range(4)}

            calc = TANGO([element],
                         DftCalc=None,
                         DftbPlusCalc=DftbPlusCalc,
                         mode=mode,
                         kptdensity=None,
                         maximum_angular_momenta=mam,
                         fit_constant='element',
                         kBT=2.0,
                         force_scaling=0.,
                         rcuts=rcuts,
                         rmins=rmins,
                         powers=powers)

            residual = calc.fit_repulsion([dbfile], run_checks=False,
                                          atomic_energies=atomic_energies,
                                          dimersdb=dimersdbfile)

            suffix = '%s_%s' % (dbext, mode)
            assert residual < 0.02, (suffix, residual)
            os.system('mv %s.pdf %s_%s.pdf' % (pair, pair, suffix))
    return
