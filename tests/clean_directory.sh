# Script for removing known artifacts from a test directory
# Note: it is not needed to run this cleanup script before running a test;
# the tests should automatically remove artifacts from prior runs where needed
rm *.db *.hsd *.bin *.gen *.pdf *.npy *.txt *.out *.png *.txt *.skf *.json
rm -r iter0* __pycache__
