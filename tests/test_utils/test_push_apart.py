import numpy as np
import pytest
from ase import Atoms
from ase.data import atomic_numbers
from ase.ga.utilities import atoms_too_close, closest_distances_generator

from tango.relax_utils import push_apart


@pytest.mark.parametrize('variable_cell', [True, False])
def test_push_apart(variable_cell):
    symbol = 'C'
    Z = atomic_numbers[symbol]
    blmin = closest_distances_generator(atom_numbers=[Z],
                                        ratio_of_covalent_radii=0.6)
    dmin = blmin[(Z, Z)]

    symbols = symbol * 4
    lx = dmin*0.9 if variable_cell else dmin*1.1
    cell = (lx, 3, 4)
    positions = np.array([
        [0.63517994, 0.85841805, 2.04166308],
        [0.51960339, 2.34811742, 3.40332555],
        [0.87702112, 2.06973158, 0.92794001],
        [0.35761118, 1.02953405, 2.56144737],
    ])
    atoms = Atoms(symbols, cell=cell, positions=positions, pbc=True)

    too_close = atoms_too_close(atoms, blmin, use_tags=False)
    assert too_close

    logfile = f'opt_push_apart_vc{variable_cell}.log'
    trajectory = f'opt_push_apart_vc{variable_cell}.traj'

    with open(logfile, 'w') as f:
        atoms = push_apart(atoms, blmin, variable_cell=variable_cell,
                           logfile=f, trajectory=trajectory)

    e = atoms.get_potential_energy()
    assert np.isscalar(e)
    e_ref = 0.012039 if variable_cell else 0.013975
    assert abs(e - e_ref) < 5e-3

    f = atoms.get_forces()
    fmax = np.max(np.sqrt(np.sum(f**2, axis=1)))
    assert fmax < 0.05

    # Certain interatomic distances may still be slightly below dmin
    blmin[(Z, Z)] = dmin * 0.99
    too_close = atoms_too_close(atoms, blmin, use_tags=False)
    assert not too_close
    return
