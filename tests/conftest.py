import os
import warnings

import pytest


def pytest_assertion_pass(item, lineno, orig, expl):
    print("asserting that {}, {}, {}, {}".format(item, lineno, orig, expl))


@pytest.fixture(autouse=True)
def change_test_dir(request, monkeypatch):
    check_invocation_dir()
    monkeypatch.chdir(request.fspath.dirname)


def check_invocation_dir():
    work_dir = os.getcwd()
    test_dir = os.path.dirname(__file__)
    equivalent = os.path.samefile(work_dir, test_dir)
    if not equivalent:
        # While pytest can be run from the project's root folder,
        # this is currently not recommended, e.g. because of possibly
        # unexpected behavior in the case of non-editable installs.
        # This is currently also needed to let pytest pick up the
        # tests/pytest.ini configuration file.
        warnings.warn('pytest should be invoked from the "tests" folder')
