"""
Quick test of a Tango run for bulk Si, with a Tersoff potential
as reference method for training the DFTB repulsion.
"""
import os
import shutil
import sys
import traceback

import numpy as np
from ase.data import atomic_numbers, covalent_radii

from tango import TANGO

from cp2k_si_bulk import CP2KCalculator
from ga_si_bulk import DftbPlusCalc, comparator, prepare_ga, run_ga
from skfgen_si_bulk import generate_skf


kptdensity = 1.5
N = None


def run(args):
    rundir, maxiter = args

    if not os.path.exists(rundir):
        os.mkdir(rundir)
    os.chdir(rundir)

    sys.stdout = open('out.txt', 'a')
    sys.stderr = open('err.txt', 'w')

    try:
        if not os.path.exists('godb.db'):
            prepare_ga(splits={(2, 2): 1, (2,): 2, (1,): 1}, N=N)
        run_ga(maxiter, kptdensity=kptdensity)
    except:  # noqa
        traceback.print_exc()
        sys.stderr.flush()
        raise

    os.chdir('..')
    return


def generator(dbfile='godb.db'):
    prepare_ga(dbfile=dbfile, splits={(2, 2): 1, (2,): 2, (1,): 1}, N=10)
    return


def create_inputs():
    """
    Ensures an iter000 directory exists and contains the needed
    repulsionless SKF file(s).
    """
    dirname = 'iter000'
    if not os.path.isdir(dirname):
        os.mkdir(dirname)
        os.chdir(dirname)
        if not os.path.exists('Si-Si_offsite2c.skf'):
            generate_skf()
        os.chdir('..')
    return


def clean_artifacts():
    """Clean the iter00[0-2] folders except for products of create_inputs()."""
    for i in range(3):
        dirname = f'iter{i:03d}'

        if i == 0:
            os.chdir(dirname)
            for item in os.listdir('.'):
                if os.path.isdir(item):
                    if item.startswith('run0'):
                        shutil.rmtree(item)
                else:
                    if not item.endswith('_offsite2c.skf'):
                        os.remove(item)
            os.chdir('..')

        elif os.path.isdir(dirname):
            shutil.rmtree(dirname)
    return


def test_si_bulk():
    global N

    elements = ['Si']
    rcov = covalent_radii[atomic_numbers['Si']]
    rcuts = {'Si-Si': 1.5 * 2 * rcov}
    rmins = {'Si-Si': 0.6 * 2 * rcov}
    powers = {'Si-Si': range(2, 4)}

    create_inputs()
    clean_artifacts()
    np.random.seed(123)

    calc = TANGO(elements,
                 DftCalc=CP2KCalculator,
                 DftbPlusCalc=DftbPlusCalc,
                 kptdensity=kptdensity,
                 initial_training='random_vc_relax',
                 generator=generator,
                 maximum_angular_momenta={'Si': 1},
                 mode='exp_poly',
                 rcuts=rcuts,
                 rmins=rmins,
                 powers=powers,
                 kBT=10.,
                 comparator=comparator,
                 max_select=20,
                 )

    N = 1
    calc.run(steps=1, go_steps=0, recalculate_dftb=True,
             run_go=run, number_of_go_runs=2, restart_go=True)
    N = 4
    calc.run(steps=1, go_steps=10, recalculate_dftb=True,
             run_go=run, number_of_go_runs=2, restart_go=True)
    return
